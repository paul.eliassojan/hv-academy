"""Given an integer numRows, return the first numRows of Pascal's triangle."""


class Solution:
    def pascal(self, numRow):
        a = [[1]*i for i in range(1, numRow+1)]
        for i in range(2, numRow):
            for j in range(1, i):
                a[i][j] = a[i-1][j-1]+a[i-1][j]
        return a


def main(row):
    out = Solution().pascal(row)
    return out


if __name__ == "__main__":
    main()
