""" Given an integer array nums, find the contiguous subarray (containing at least one number) which has the 
largest sum and return its sum """


class Solution:
    def maxSubArray(self, nums):
        max = nums[0]
        sum = 0

        for i in range(0, len(nums)):
            sum += nums[i]
            if sum > max:
                max = sum
            elif sum < 0:
                sum = 0
        return max

def main(nums):
    # n = int(input("Enter the range: "))
    # nums = []
    # for i in range(0, n):
    #     ele = int(input())
    #     nums.append(ele)
    out = Solution().maxSubArray(nums)
    return out

if __name__=="__main__":
    main()