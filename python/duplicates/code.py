"""Given an integer array nums, return true if any value appears at least twice in the array, and
return false if every element is distinct."""


class Solution:
    def duplicate(self, nums):
        nums.sort()
        flag = 0
        for i in range(0, len(nums)):
            if((i+1) < len(nums)):
                if(nums[i] == nums[i+1]):
                    flag = 1
                    break
        if(flag == 1):
            return True

        else:
            return False


def main(nums):

    out = Solution().duplicate(nums)
    return out


if __name__ == "__main__":
    main()
