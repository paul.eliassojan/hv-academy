"""
Form a number divisible by 3 using array digits
"""


class Solution:
    """
    Return Yes/No if number is divisible by 3
    """
    @classmethod
    def divisible(cls, nums):
        count = len(nums)
        sum_arr = 0
        for i in range(0, count):
            sum_arr = sum_arr+nums[i]

        if sum % 3 == 0:
            return 'Yes'

        return 'No'


def main(nums):
    """
    Return result from class
    """
    out = Solution().divisible(nums)
    return out


if __name__ == "__main__":
    main()
