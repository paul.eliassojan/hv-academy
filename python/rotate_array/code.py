class Solution:
    """
    Returns the rotated array
    """
    @classmethod
    def rotate(cls, num_arr, rotate):
        """
        Rotate anti-clockwise
        """
        count = len(num_arr)
        temp = []
        for i in range(0, count):
            temp.append(num_arr[(i+rotate) % count])

        return temp


def main(num_arr, rotate):
    out = Solution().rotate(num_arr, rotate)
    return out


if __name__ == "__main__":
    main()
