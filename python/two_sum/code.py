"""Given an array of integers nums and an integer target, return indices of the two numbers such that they
add up to target."""


class Solution:
    def __init__(self):
        pass

    def two_sum(self, nums, target):
        a = []
        for i in range(0, len(nums)):
            for j in range(i+1, len(nums)):
                if(nums[i]+nums[j] == target):
                    a.append(i)
                    a.append(j)
        return a


def main(nums, target):
    # n = int(input("Enter the range: "))
    # nums = []
    # for i in range(0, n):
    #     ele = int(input())
    #     nums.append(ele)
    # target = int(input("Enter the target value"))
    out = Solution().two_sum(nums, target)
    # print("Result:", out)
    return out


if __name__ == "__main__":
    main()
